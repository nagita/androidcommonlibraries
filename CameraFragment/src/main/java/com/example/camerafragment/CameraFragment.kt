package com.example.camerafragment

import android.Manifest
import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.ImageFormat
import android.graphics.PixelFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.hardware.camera2.params.StreamConfigurationMap
import android.media.ImageReader
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.util.*
import java.util.concurrent.Semaphore

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CameraFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CameraFragment : Fragment(), ActivityCompat.OnRequestPermissionsResultCallback {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    // for fragment variables
    private lateinit var mTextureView: TextureView
    private lateinit var mSurfaceView: SurfaceView

    // for camera device
    private var mImageReader: ImageReader? = null
    private var mCameraDevice: CameraDevice? = null
    private var mCaptureSession: CameraCaptureSession? = null
    private lateinit var mSurfacePreviewSize: Size
    private lateinit var mPreviewRequestBuilder: CaptureRequest.Builder
    private lateinit var mPreviewRequest: CaptureRequest
    private var mCameraSupportedPreviewSize: MutableList<Size> = arrayListOf()

    private val previewSize: Size = Size(300, 300) // FIXME: for now.

    // for task
    private var mBackgroundHandlerThread: HandlerThread? = null
    private var mBackgroundHandler: Handler? = null
    private var mSemaphore: Semaphore = Semaphore(SEMAPHORE_PERMITS_COUNT)

    private val mCameraOpenCloseLock = Semaphore(1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


        startBackgroundThread()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // this function will call after onCreate() and before onViewCreated()
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_camera, container, false)
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {

        } else {
            val cameraSurfaceView: SurfaceView = view!!.findViewById(R.id.CameraSurfaceView)
            cameraSurfaceView.layoutParams.width = 1280
            cameraSurfaceView.layoutParams.height = 720
            cameraSurfaceView.requestLayout()

            val cameraTextureView: TextureView = view!!.findViewById(R.id.CameraTextureView)
            cameraTextureView.layoutParams.width = 1280
            cameraTextureView.layoutParams.height = 720
            cameraTextureView.requestLayout()
        }



        mTextureView = view!!.findViewById(R.id.CameraTextureView)
        mTextureView.surfaceTextureListener = surfaceTextureListener



        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_camera, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSurfaceView = view.findViewById(R.id.CameraSurfaceView)
        mSurfaceView.setZOrderOnTop(true)
        val mHolder = mSurfaceView.holder
        mHolder.setFormat(PixelFormat.TRANSPARENT)
        mHolder.addCallback(surfaceHolderCallback)
    }

    override fun onPause() {
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        startBackgroundThread()
        if (mTextureView.isAvailable) {
            if (mImageReader == null) {
                mImageReader = ImageReader.newInstance(
                    mSurfacePreviewSize.width, mSurfacePreviewSize.height, ImageFormat.JPEG, 2)
            }
            openCamera()
        } else {
            if (mTextureView.surfaceTextureListener == null) {
                mTextureView.surfaceTextureListener = surfaceTextureListener
            }
        }
    }

    fun openCamera() {
        val manager: CameraManager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager

        try {
            val cameraID: String = manager.cameraIdList[CAMERA_ID]
            val permission = ContextCompat.checkSelfPermission(activity!!.applicationContext, Manifest.permission.CAMERA)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission()
                return
            }
            manager.openCamera(cameraID, stateCallback, null);
        } catch (e: CameraAccessException) {
            Log.e("CF", e.toString())
            e.printStackTrace()
        } catch (e: InterruptedException) {
            Log.e("CF", e.toString())
            e.printStackTrace()
        } catch (e: Exception) {
            Log.e("CF", e.toString())
            e.printStackTrace()
        }
    }

    private fun closeCamera() {
        try {
            mCaptureSession?.let {
                mCaptureSession!!.close()
                mCaptureSession = null
            }

            mCameraDevice?.let {
                mCameraDevice!!.close()
                mCameraDevice = null
            }

            mImageReader?.let {
                mImageReader!!.close()
                mImageReader = null
            }
        } catch (e: InterruptedException) {
            throw java.lang.RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            mCameraOpenCloseLock.release()
        }
    }

    private val surfaceHolderCallback = object: SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder?) {
        }

        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
        }
    }

    private val stateCallback = object : CameraDevice.StateCallback() {

        override fun onOpened(cameraDevice: CameraDevice) {
            this@CameraFragment.mCameraDevice = cameraDevice
            mCameraOpenCloseLock.release()
            mCameraDevice = cameraDevice
            createCameraPreviewSession()
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            mCameraOpenCloseLock.release()
            cameraDevice.close()
            this@CameraFragment.mCameraDevice = null
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            onDisconnected(cameraDevice)
            Log.e("CameraFragment", "Camera get ERROR !!")
            this@CameraFragment.activity?.finish()
        }
    }

    private fun createCameraPreviewSession() {
        try {
            val texture = mTextureView.surfaceTexture
            texture.setDefaultBufferSize(mSurfacePreviewSize.width, mSurfacePreviewSize.height)
            val surface = Surface(texture)
            mPreviewRequestBuilder = mCameraDevice!!.createCaptureRequest(
                CameraDevice.TEMPLATE_PREVIEW
            )
            mPreviewRequestBuilder.addTarget(surface)
            mCameraDevice?.createCaptureSession(
                mutableListOf(surface, mImageReader?.surface),
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                object : CameraCaptureSession.StateCallback() {
                    override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                        if (mCameraDevice == null) return
                        mCaptureSession = cameraCaptureSession
                        try {
                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                            mPreviewRequest = mPreviewRequestBuilder.build()
                            mCaptureSession?.setRepeatingRequest(mPreviewRequest,
                                null, Handler(mBackgroundHandlerThread?.looper!!))
                        } catch (e: CameraAccessException) {
                            Log.e("erfs", e.toString())
                        } catch (e: NullPointerException) {
                            Log.e("erfs", e.toString())
                        } catch (e: java.lang.Exception) {
                            Log.e("erfs", e.toString())
                        }

                    }

                    override fun onConfigureFailed(session: CameraCaptureSession) {
//                        Toast.makeText(mContext, "Camera Capture Session Configure Failed", Toast.LENGTH_SHORT).show()
                    }
                }, null)
        } catch (e: CameraAccessException) {
            Log.e("erf", e.toString())
            e.printStackTrace()
        } catch (e: NullPointerException) {
            Log.e("erfs", e.toString())
            e.printStackTrace()
        } catch (e: java.lang.Exception) {
            Log.e("erfs", e.toString())
            e.printStackTrace()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            AlertDialog.Builder(activity!!.applicationContext)
                .setMessage("Permission Here")
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
                }
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    // TODO: can not get camera permission, need to do something
                    Log.e("CameraFragment", "Can not get camera permission, need to do something")
                }
                .create()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        }
    }

    private fun startBackgroundThread() {
        if (mBackgroundHandlerThread == null) {
            mBackgroundHandlerThread = HandlerThread("CameraBackground").also { it.start() }
        }

        if (mBackgroundHandler == null) {
            mBackgroundHandler = Handler(mBackgroundHandlerThread?.looper!!)
        }
    }

    private fun stopBackgroundThread() {
        mBackgroundHandlerThread?.quitSafely()
        try {
            mBackgroundHandlerThread?.join()
            mBackgroundHandlerThread = null
            mBackgroundHandler?.removeCallbacks(null)
            mBackgroundHandler = null
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {

        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
            val cameraManager: CameraManager = mTextureView.context.getSystemService(Context.CAMERA_SERVICE) as CameraManager

            val streamConfigurationMap: StreamConfigurationMap? =
                cameraManager.getCameraCharacteristics(cameraManager.cameraIdList[CAMERA_ID]).get(
                    CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP) ?: return
            mCameraSupportedPreviewSize.clear()
            for (outputSize in streamConfigurationMap!!.getOutputSizes(ImageFormat.JPEG)) {
                Log.i("CF", "preview size: $outputSize")
                mCameraSupportedPreviewSize.add(outputSize)
            }
            mCameraSupportedPreviewSize.sortBy { it.width * it.height }
            if (mImageReader == null) {
                val previewSize = geLargestPreviewSize(Size(CAMERA_PREVIEW_SIZE_WIDTH, CAMERA_PREVIEW_SIZE_HEIGHT))
                mImageReader = ImageReader.newInstance(
                    previewSize.width, previewSize.height, ImageFormat.JPEG, 2)
            }
            openCamera()
            mSurfacePreviewSize = geLargestPreviewSize(Size(CAMERA_PREVIEW_SIZE_WIDTH, CAMERA_PREVIEW_SIZE_HEIGHT))
            Log.i("CF", "Surface Preview Size: (${mSurfacePreviewSize.width}, ${mSurfacePreviewSize.height})")
        }

        override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture?, p1: Int, p2: Int) {

        }

        override fun onSurfaceTextureUpdated(p0: SurfaceTexture?) {

        }

        override fun onSurfaceTextureDestroyed(p0: SurfaceTexture?): Boolean {
            return false
        }
    }

    private fun geLargestPreviewSize(requestSize: Size): Size {
        var size: Size = Size(-1, -1)
        mCameraSupportedPreviewSize.forEach {
            if (it.width <= requestSize.width && it.height <= requestSize.height) {
                if (size.width < it.width && size.height < it.height) {
                    size = it
                }
            }
        }
        return size
    }

    companion object {
        const val CAMERA_ID: Int = 0
        const val REQUEST_CAMERA_PERMISSION: Int = 1
        const val REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION : Int = 2
        const val CAMERA_REQUEST_CODE : Int = 3

        const val SEMAPHORE_PERMITS_COUNT: Int = 1

        const val CAMERA_PREVIEW_SIZE_WIDTH: Int = 1920//1280
        const val CAMERA_PREVIEW_SIZE_HEIGHT: Int = 1080//720
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CameraFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CameraFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}